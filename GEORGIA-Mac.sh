#!/usr/local/bin/bash

cat << "Startup"




     ,o888888o.    8 8888888888       ,o888888o.     8 888888888o.      ,o888888o.     8 8888          .8.
    8888     `88.  8 8888          . 8888     `88.   8 8888    `88.    8888     `88.   8 8888         .888.
 ,8 8888       `8. 8 8888         ,8 8888       `8b  8 8888     `88 ,8 8888       `8.  8 8888        :88888.
 88 8888           8 8888         88 8888        `8b 8 8888     ,88 88 8888            8 8888       . `88888.
 88 8888           8 888888888888 88 8888         88 8 8888.   ,88' 88 8888            8 8888      .8. `88888.
 88 8888           8 8888         88 8888         88 8 888888888P'  88 8888            8 8888     .8`8. `88888.
 88 8888   8888888 8 8888         88 8888        ,8P 8 8888`8b      88 8888   8888888  8 8888    .8' `8. `88888.
 `8 8888       .8' 8 8888         `8 8888       ,8P  8 8888 `8b.    `8 8888       .8'  8 8888   .8'   `8. `88888.
    8888     ,88'  8 8888          ` 8888     ,88'   8 8888   `8b.     8888     ,88'   8 8888  .888888888. `88888.
     `8888888P'    8 888888888888     `8888888P'     8 8888     `88.    `8888888P'     8 8888 .8'       `8. `88888.

Startup

echo Updating GEORGIA with changes made to upstream.
git fetch --all
git reset --hard origin/master
git pull

echo "Welcome to GEORGIA! Please enter the filename of the flag dump (.xml) you wish to download, then press ENTER."
read xmlToRead
mapfile -t listOfFlags < "$xmlToRead"

flagsToDownload=$(grep 'https://gitlab.com/flagtism/Extra-Flags-for-4chan/raw/master/flags' $xmlToRead)
updatedString=$(xmlstarlet sel -t -v '//imgurl' "$xmlToRead")

readarray -t testArray <<<"$updatedString"

for file in "${testArray[@]}"
do		
		if [[ "$file" =~ ".png" ]];
		then
			# Logic to manipulate substring, not fully-operational yet
			filePath=$(echo "${file%/*}")
			wget "$file" -P "$filePath"
		else
			wget "$file"
		fi
done
