# GEORGIA

**G**et **E**very **O**riginal **R**egional, **G**et **I**t **A**utomatically is a simple bash utility to parse flag dumps and save their contents.

[![ezgif-7-399ca38c7054.gif](https://i.postimg.cc/QxXs1KKd/ezgif-7-399ca38c7054.gif)](https://postimg.cc/BtwdG6Cr)

# Requirements

*   Ability to run bash files. Linux or MacOS is recommended.
*   [git](https://git-scm.com/) is required for version control and automatic updates.
*   [XMLStarlet](http://xmlstar.sourceforge.net/docs.php) is necessary to parse the xml.
*   If using Mac, install `wget`. If using Linux, it's likely preinstalled.

# Usage

1.  Clone this directory to a safe location on your computer using the following terminal command: `git clone https://gitlab.com/Tampanon/GEORGIA`
2.  Enter the cloned directory: `cd GEORGIA`  
3.  Make the script executable: `chmod +x GEORGIA.sh`
4.  Place the parsed .xml file in this directory (Trieste posts a link to the latest .xml every Wednesday)
5.  Run the script: `./GEORGIA.sh`
6.  Follow the on-screen instructions!

# Known Issues

Currently it's saving the regional flags within an ugly directory consisting of the gitlab URL hierarchy. This isn't a huge deal to me at the moment since it's easy to click through to the proper folder, but I will fix it sometime when I feel like it.
